// #include <avr/interrupt.h>

/*
 * pwm.h
 *
 *  Created on: Jan 28, 2022
 *      Author: aroni
 */
#ifndef __pwm__
#define __pwm__
void pwm_init();
void pwm(int);
// ISR(TIMER0_OVF_vect);
#endif

