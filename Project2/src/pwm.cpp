/*
 * pwm.c
 *
 *  Created on: Jan 28, 2022
 *      Author: aroni
 */
#include "pwm.h"
#include <avr/interrupt.h>

#include <avr/io.h>
int duty_cycle;
int intr_duty_cycle;
void pwm_init(){
    TCCR0A = (1<< COM0A1) | (1 << WGM00) | (1 << WGM01);
    TIMSK0 = (1 << TOIE0);
    asm("SEI");
    TCCR0B = (1 << CS00)|(1 << CS02);
    DDRD |= 1 << 6;

}

void pwm(int duty_cycle)
{
	intr_duty_cycle = duty_cycle;
    OCR0A = (duty_cycle); //Put the correct duty cycle into the PWM register (simplified)
}

//Interrupt so that the PWM signal can be changed.
ISR(TIMER0_OVF_vect)
{
	OCR0A = (intr_duty_cycle);
}

