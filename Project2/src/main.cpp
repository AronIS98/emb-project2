#include <avr/interrupt.h>
#include <util/delay.h>
#include "digital_out.h"
#include "digital_in.h"
#include "encoder.h"
#include <Arduino.h>
#include "controler.h"

volatile int enc_position = 0;
volatile int speed_counter = 0;
volatile int speed_timer = 10;
volatile double speed = 1.0;
int desired_speed = 1000;
int curr_pwm;

  Digital_out led(5);
  Digital_out motor(1);
  Encoder enc;
  Controler contr;

char TxBuffer[32];
int indx, len;
// int encoder = 0;

void Init_Uart()
{
    // 57600 baudrate
    UBRR0H = 0;
    UBRR0L = 16;
    UCSR0B = (1<<RXEN0) |(1<<TXEN0);// |(1<<TXCIE0);
    UCSR0C = (1<<USBS0) |(3<<UCSZ00);
    indx = len = 0;
}

void reset_TxBuffer(){
    indx = len = 0;
}

void UART_transmit_TxBuffer(){
    while (indx < len){
        while(!(UCSR0A & (1<<UDRE0))){;}
        UDR0 = TxBuffer[indx];
        indx++;
    }
}


int strlen(char s[])
{
    int i=0;
    while ( s[i] != 0 )
    i++;
    return i;
}

void reverse(char s[])
{
    int i,j;
    char c;

    for (i=0, j=strlen(s)-1; i<j; i++, j--){
    c = s[i];
    s[i] = s[j];
    s[j] = c;
    }
}

void UART_itoa(int n, char s[])
{
    int i,sign;

    if ( (sign = n ) < 0 )
        n = -n;
    i = 0;
    do {
        s[i++] = n % 10 + '0';
    } while ( ( n /= 10 ) > 0 );
    if (sign < 0 )
    s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

void UART_load_string_in_TxBuffer(char s[])
{
    while(s[len] != 0)
    {
        TxBuffer[len] = s[len];
        len++;
    }

}

void UART_load_charVal_in_TxBuffer(int data)
{
    char temp[32];
    UART_itoa(data,temp);
    UART_load_string_in_TxBuffer(temp);
}





int main()
{
  DDRD |= 1<<PD1;
  DDRB |= 1<<PB0;
  PORTD |= 1<<PD7;
  PORTB &= ~1<<PB0;

  enc.timer_msec(speed_timer);
  enc.init();
  led.set_lo();
  motor.set_lo();

  enc.pwm_init();
  Init_Uart();
  int curr_pwm;
  DDRD |= 1 << 6;
  int counter = 0;

  while(1)
  {
  
    curr_pwm = contr.update(desired_speed,speed);
    enc.pwm_update(curr_pwm);
    _delay_ms(4);


    if(counter > 500) //Print desired values.
    { UART_load_string_in_TxBuffer("Measured speed: ");
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer(); // reset transmit buffer 
      _delay_ms(1);
      UART_load_charVal_in_TxBuffer(speed); // load encoder value to transmit
      TxBuffer[len] = '\r';
        TxBuffer[len+1] = '\n';
        len += 2;
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer(); // reset transmit buffer 
      _delay_ms(1);

      UART_load_string_in_TxBuffer("Target speed: ");
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer();
      UART_load_charVal_in_TxBuffer(desired_speed); // load encoder value to transmit
      TxBuffer[len] = '\r';
        TxBuffer[len+1] = '\n';
        len += 2;
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer(); // reset transmit buffer 
      _delay_ms(1);
      
      UART_load_string_in_TxBuffer("PWM: ");
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer();
      UART_load_charVal_in_TxBuffer(curr_pwm); // load encoder value to transmit
      TxBuffer[len] = '\r';
        TxBuffer[len+1] = '\n';
        TxBuffer[len+2] = '\n';
        len += 3;
      UART_transmit_TxBuffer(); // transmit encoder value over UART
      reset_TxBuffer(); // reset transmit buffer 
      counter = 0;
    }
    counter++;
  }


  return 1;
}

ISR(INT0_vect)
{

  DDRD &= ~(1<<3);
  if(((PIND>>3) & 1))
  {
    enc_position++;
    speed_counter++;
  }
  else
  {
    enc_position--;
    speed_counter--;
  }
}

ISR(TIMER1_COMPA_vect)
{
  DDRB |= 1<<5;
  PORTB ^= 1<<5;
  speed = ((speed_counter*1.0)*1000.0)/((speed_timer*1.0));
  speed_counter = 0;

}

ISR(TIMER0_COMPA_vect)
{
  
  PORTD |= 1 << 6;
}

ISR(TIMER0_COMPB_vect)
{
  PORTD &= !1 << 6;;
}