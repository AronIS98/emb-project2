/*
 * encoder.c
 *
 *  Created on: Jan 31, 2022
 *      Author: aroni
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "encoder.h"

//Variables:
bool old_int0 = false;
bool old_int1 = false;
int temp_pwm_val;
float duty_glob;
int pwm_signal;

Encoder::Encoder(){
  counter = 0;
  current_enc2 = false;
  current_enc1 = false;
  old_enc1 = false;
  old_enc2 = false;
  enc1_change = false;
  enc2_change = false;

    
}

void Encoder::init(){
	DDRD &= ~(1 << DDD2); // set the PD2 pin as input
  DDRD &= ~(1 << DDD3); // set the PD3 pin as input
  PORTD |= (1 << PORTD2); // enable pull-up resistor on PD2
  PORTD |= (1 << PORTD3); // enable pull-up resistor on PD3
  EICRA |= (1 << ISC00) | (1 << ISC01); // set INT0 to trigger on RISING edge.
  EIMSK |= (1 << INT0);
  sei(); // turn on interrupts

}

int Encoder::position()
{
    return counter;
}

void Encoder::read_encoder(bool encoder1, bool encoder2){
    current_enc1 = encoder1;
    current_enc2 = encoder2;

    enc1_change = (current_enc1 != old_enc1);
    enc2_change = (current_enc2 != old_enc2);

    if((enc1_change & !current_enc2)& current_enc1){
      counter--;

      }
    else if((enc2_change & !current_enc1)& current_enc2){
      counter++;
      }

    old_enc1 = current_enc1;
    old_enc2 = current_enc2;
    
}

void Encoder::timer_msec(int per)
{
    // this code sets up timer1 for a 1s @ 16Mhz Clock (mode 4)
    // counting 16000000/1024 cycles of a clock prescaled by 1024
    TCCR1A = 0; // set timer1 to normal operation (all bits in control registers A and B set to zero)
    TCCR1B = 0; //
    TCNT1 = 0; //  initialize counter value to 0

    OCR1A = (per * 16000.0) / 1024 - 1; // assign target count to compare register A (must be less than 65536)
    TCCR1B |= (1 << WGM12); // clear the timer on compare match A
    TIMSK1 |= (1 << OCIE1A); // set interrupt on compare match A
    TCCR1B |= (1 << CS12) | (1 << CS10); // set prescaler to 1024 and start the timer

}


void Encoder::pwm_update(int us)
{
    OCR0A = 255-us;
}

void Encoder::pwm_init()
{
    TCCR0A = 0;
    TCCR0B = 0;
    TCNT0 = 0;
    OCR0A = pwm_signal;
    OCR0B = 0;
    TCCR0A |= (1 << WGM01);
    TIMSK0 |= (1 << OCIE0A) | (1 << OCIE0B);
    TCCR0B |= (0 << CS02) | (0 << CS01) | (1 << CS00);
    DDRD |= 1 << 6;
}